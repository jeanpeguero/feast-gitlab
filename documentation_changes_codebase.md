As said in the main [Readme.md](README.md#changes-to-the-codebase), all custom changes done to the codebase have been marked with the `# GITLAB CHANGES` comment.

Main changes are:

1. Added the `is_template_table` attribute to the `FeatureViewQueryContext` data class.
    * This allows us to know if the feature view will be computed using the newly introduced methology.
2. Check if the string `'USE_TEMPLATE_WORKFLOW'` can be found in the SQL file that correspond to the feature view. This way we can determine if the feature view uses the newly introduced methodology or not.
3. In the snowflake offline store file, in the `get_historical_features` function, added a check for the feature view to confirm it is using the template workflow (or not). If it is, then it uses Jinja to execute the template with `get_historical_features=True`.
    * This allow us to get access to the entity_dataframe at feature retrieval time.
4. Change the snowflake `MULTIPLE_FEATURE_VIEW_POINT_IN_TIME_JOIN` query. This query controls how the feature views are used to get the features that were asked.
    * One of the most important changes as it handles how the new workflow will get the features and how everything works together with feature retrievals done not using the new methodology.
5. When Feast runs in validation mode (checking that the data sources and feature views definitions are valid), it checks if the table is using the new template workflow. Does this by checking if the `'-- USE_TEMPLATE_WORKFLOW'` string is in the query. If it is, it first templates the query with jinja passing the variable `validation=True`.
     * This allo us to massively speed up validation as we can add a validation clause to the query where we limit the size of the biggest table, making sure the transformations are SQL valid and can be ran but no needed to be ran in the entire dataset.
